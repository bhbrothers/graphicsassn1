[프로그램 조작법]
- Keyboard Input

[q]		: 행성 공전속도 증가(공전 및 자전 중지 상태일 경우에는 값만 증가하고 중지 상태를 

유지함)
[a]		: 행성 공전속도 증가(공전 및 자전 중지 상태일 경우에는 값만 감소하고 중지 상태를 

유지함)
[s]		: 공전 및 자전 중지/시작( 토글버튼)
[w]		: 공전 및 자전 시작(중지 상태 직전 속도로 자전 시작)
[e]		: 행성 자전속도 증가(공전 및 자전 중지 상태일 경우에는 값만 증가하고 중지 상태를 

유지함)
[d]		: 행성 자전속도 감소(공전 및 자전 중지 상태일 경우에는 값만 감소하고 중지 상태를 

유지함)
[r]		: 모든 값 초기화(공전 및 자전 중지 상태일 경우에는 값만 리셋하고 중지 상태를 유
[SPACE]	: 우주선 발사
[z]		: zoom in - Orthogonal Mode인 경우에만 해당
[x]		: zoom out - Orthogonal Mode인 경우에만 해당
[c]		: 지구의 구름 보이기/가리기
[o[		: 궤도&행성선택큐브 보이기/없애기
[방향키]: 카메라 이동
[p]	: Projection Toggle
[ESC]		: 프로그램 종료

[마우스 행성 클릭] : 행성의 움직임 추적
[마우스 드래그]    : 카메라가 보는 방향 변경
[마우스 빈 곳 클릭]: 행성의 움직임 추적 종료

[File Description]
- source file
	main.cpp 	: 행성 운동 프로그램
	InitShader.cpp 	: 셰이더 처리
	vertex.glsl	: vertex shader 
	fragment.glsl	: fragment shader
	select.glsl	: 행성 추적용 shader
        Control.cpp: 키보드/애니메이션 컨트롤 class
        Camera.cpp : 카메라 조작 class
        Object.cpp : Object 부모 class
        Planet.cpp : 행성 class (태양, 행성, 위성)
        ObjectLoader.cpp : Obj Loader class
	Earth.cpp	: 지구 클래스(Planet 상속)

- header file
	Angel.h 	: 셰이더 처리, vec.h, mat.h 등의 프로그램에 쓰이는 대부분의 기능 포함
	CheckError.h	: 에러 체킹용
	mat.h		: matrix 타입 정의
	vec.h		: vector 타입 정의
	main.h          : global variables
	Control.h: 키보드/애니메이션 컨트롤 class header file
        Camera.h : 카메라 조작 class header file
        Object.h : Object 부모 class header file
        Planet.h : 행성 class header file
	ObjectLoader.h : Obj Loader header file
	Earth.h		: 지구 class header file

- external library
	SOIL		: openGL image library(텍스처 로딩 용도)
	

- resource files	
	planet_textures/*	: 텍스처 파일들
	TexSphere.obj		: 행성 오브젝트
	Track.obj		: 트랙 오브젝트
	cube.obj		: 큐브 오브젝트(와이어프레임)