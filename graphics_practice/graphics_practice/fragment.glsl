#version 440
// per-fragment interpolated values from the vertex shader
in  vec3 fN;
in  vec3 fL;
in  vec3 fE;
in  vec2 texCoord;
in  vec2 cTexCoord;
in	vec3 fTangent;
in	vec3 fBitangent;

out vec4 fColor;

uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform mat4 ModelView;
uniform vec4 LightPosition;
uniform float Shininess;
uniform int IsShading;
uniform sampler2D TextureColor;
uniform sampler2D TextureColorNight;
uniform sampler2D TextureNormalMap;
uniform sampler2D TextureSpec;
uniform sampler2D TextureCloud;
uniform int UseTexture;
uniform int IsEarth;
uniform int EnableCloud;

void main() 
{ 
	if(IsShading == 1 ){
		// Normalize the input lighting vectors
		vec3 N = normalize(fN);
		vec3 E = normalize(fE);
		vec3 L = normalize(fL);
		vec4 texColor = texture2D(TextureColor, texCoord.st);
		float shine = Shininess;

		// Specular effect
		vec3 H = normalize( L + E );
		

		if(IsEarth == 1){
			//Normal mapping
			 vec3 map = normalize(texture2D(TextureNormalMap, texCoord.st).rgb * 2.0 - 1.0);
			 vec3 dp1 = dFdx(E);
			 vec3 dp2 = dFdy(E);
			 vec2 duv1 = dFdx(texCoord);
			 vec2 duv2 = dFdy(texCoord);

			 vec3 dp2perp = cross(dp2, N);
			 vec3 dp1perp = cross(N, dp1);
			 vec3 T = normalize(dp2perp * duv1.x + dp1perp * duv2.x);
			 vec3 B = normalize(dp2perp * duv1.y + dp1perp * duv2.y);
			 float invmax = inversesqrt(max(dot(T,T), dot(B,B)));
			 mat3 TBN = mat3(T * invmax, B * invmax, N);
			 N = normalize(TBN * map);
			//N = normalize( N + normalize(texture2D(TextureNormalMap, texCoord.st).rgb * 2.0 - 1.0) );
		
			//Specular mapping
			shine =  (normalize(texture2D(TextureSpec, texCoord.st)).r) > 0.1 ? 3 : 150;
		}
		
		vec4 ambient = AmbientProduct * texColor;

		float Kd = max(dot(L, N), 0.0);
		
		vec4 diffuse = Kd * texColor;
		float Ks = pow(max(dot(N, H), 0.0), shine);
		vec4 specular = Ks*SpecularProduct;


		if(IsEarth == 1){
			float Knd = 1-Kd;
			Knd = pow (Knd, 4);

			vec4 nightColor = texture2D(TextureColorNight, texCoord.st);
			vec4 night = Knd * nightColor;

			vec4 cloud = vec4(0, 0, 0, 0);
			if(EnableCloud > 0)
			{
				//Cloud mapping
				vec4 texCloud = texture2D(TextureCloud, cTexCoord.st);
				cloud = Kd * texCloud + Knd * texCloud * 0.2;
			}
			
			if( night.xyz.x < 0.3 && night.xyz.y < 0.3 && night.xyz.z < 0.3 )
			{
				night *= vec4(0.3,0.5,0.3,1.0);
			}


			fColor = ambient + (diffuse + night + cloud) + specular;
		}
		else
		{
			fColor = ambient + diffuse + specular;
		}

		fColor.a = 1.0;

	}
	else {
		fColor = vec4(1.0, 1.0, 1.0, 1.0);
		if(UseTexture == 1){
			vec4 texColor = texture(TextureColor, texCoord);
			fColor = fColor * texColor;
		}
	}
} 
