#include "main.h"
#include "Camera.h"
#include "Control.h"
#include "Planet.h"
#include "Earth.h"
#include "MatrixStack.h"
#include "SOIL.h"
#include <cstdlib>

Control control;
vector<vec4> vertices;
vector<vec4> colors;
vector<vec3> normals;
vector<vec2> textures;

ObjectLoader planetObjLoader;
ObjectLoader orbitObjLoader;
ObjectLoader cubeObjLoader;
GLuint program;
GLuint selectProgram;

int selectedPlanet;
int orbitToggle; // orbit 보여주기 유무
int enableCloud;
float earthTimer;

MatrixStack mvstack;
mat4 model_view, projection;
mat4 look_at;

GLuint ModelView, Projection, LookAtMat;
GLuint sModelView, sProjection, sLookAtMat;
GLuint sCode;
GLuint textureLocation, textureLocationNight, textureNormalMap, textureSpec, textureCloud, EarthTime, EnableCloud;
GLuint AmbientProduct, DiffuseProduct, SpecularProduct, LightPosition, Shininess;
GLuint IsShading;
GLuint IsEarth;
GLuint UseTexture;
Object* sceneObject;

void reshape(int w, int h)
{
	glLoadIdentity();
	glViewport(0, 0, w, h);
	camera.setRatio(w, h);
}

void idle()
{
	glutPostRedisplay();
}

void drawScene(bool isSelect)
{
	projection = camera.projection();
	look_at = camera.lookAt();

	if (isSelect)
	{
		glUseProgram(selectProgram);
		glUniformMatrix4fv(sProjection, 1, GL_TRUE, projection);
		glUniformMatrix4fv(sLookAtMat, 1, GL_TRUE, look_at);
	}
	else
	{
		glUseProgram(program);
		glUniformMatrix4fv(Projection, 1, GL_TRUE, projection);
		glUniformMatrix4fv(LookAtMat, 1, GL_TRUE, look_at);
		earthTimer += 0.0001;
		glUniform2f(EarthTime, earthTimer, 0);


		vec4 light_position = vec4(0, 0, 0, 1.0);
		vec4 light_ambient(0.1, 0.1, 0.1, 1.0);
		vec4 light_diffuse(0.8, 0.8, 0.8, 1.0);
		vec4 light_specular(0.35, 0.35, 0.35, 1.0);

		vec4 material_ambient(1.0, 1.0, 1.0, 1.0);
		vec4 material_diffuse(1.0, 1.0, 1.0, 1.0);
		vec4 material_specular(1.0, 1.0, 1.0, 1.0);
		float material_shininess = 10;

		vec4 ambient_product = light_ambient * material_ambient;
		vec4 diffuse_product = light_diffuse * material_diffuse;
		vec4 specular_product = light_specular * material_specular;


		glUniform4fv(AmbientProduct, 1, ambient_product);
		glUniform4fv(DiffuseProduct, 1, diffuse_product);
		glUniform4fv(SpecularProduct, 1, specular_product);
		glUniform4fv(LightPosition, 1, light_position);
		glUniform1f(Shininess, material_shininess);
	}

	//cout << "Ambient Product: " << ambient_product << endl;
	//cout << "Diffuse Product: " << diffuse_product << endl;
	//cout << "Specular Product: " << specular_product << endl;

	sceneObject->traverse(isSelect);
}
int processSelection(int xx, int yy) 
{

	unsigned char res[4];
	GLint viewport[4];

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	drawScene(true);

	glGetIntegerv(GL_VIEWPORT, viewport);
	glReadPixels(xx, viewport[3] - yy, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &res);

	printf("selected:%d\n", res[0]);

	return res[0];
}
void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	drawScene(false);

	glutSwapBuffers();
}

void init()
{
	orbitToggle = 1;
	enableCloud = 1;
	earthTimer = 0;

	srand(time(NULL));
	planetObjLoader.readObjFile("TexSphere.obj");
	orbitObjLoader.readObjFile("Track.obj");
	cubeObjLoader.readObjFile("Cube.obj");
	//texture = planetObjLoader.readTexture("texture_mars.jpg");

	Planet::planetVertices = 3 * planetObjLoader.faces.size();
	Planet::orbitVertices = 3 * orbitObjLoader.faces.size();

	//Planets on Solar System
	sceneObject = new Planet(RGB(255, 190, 0), 1.0f, 0, 0, 0, "planet_textures/texture_sun.jpg"); //Sun
	sceneObject->setm(RotateX(90));
	Planet* mercury = new Planet(RGB(210, 155, 90), 0.167f, 2.5f, 0.3f, 2.8f, "planet_textures/texture_mercury.jpg");  //수성                                        
	Planet* venus = new Planet(RGB(235, 220, 90), 0.233f, 3.5f, 0.4f, -4.6f, "planet_textures/texture_venus_surface.jpg"); //금성
	Earth* earth = new Earth(RGB(90, 130, 230), 0.333f, 5.0f, 0.2f, 2.24f); //지구
	Planet* mars = new Planet(RGB(180, 90, 35), 0.208f, 6.0f, 1.35f, 4.26f, "planet_textures/texture_mars.jpg"); //화성
	Planet* jupiter = new Planet(RGB(255, 180, 140), 0.667f, 8.5f, 0.34f, 1.4f, "planet_textures/texture_jupiter.jpg");  //목성
	Planet* saturn = new Planet(RGB(180, 145, 50), 0.6f, 10.0f, 0.24f, 1.5f, "planet_textures/texture_saturn.jpg"); //토성
	Planet* uranus = new Planet(RGB(80, 180, 255), 0.367, 13.0f, 0.03f, 5.22f, "planet_textures/texture_uranus.jpg"); //천왕성
	Planet* neptune = new Planet(RGB(50, 130, 200), 0.333f, 14.5f, 0.02f, -6.12f, "planet_textures/texture_neptune.jpg");  //해왕성
	sceneObject->addChild(mercury);
	sceneObject->addChild(venus);
	sceneObject->addChild(earth);
	sceneObject->addChild(mars);
	sceneObject->addChild(jupiter);
	sceneObject->addChild(saturn);
	sceneObject->addChild(uranus);
	sceneObject->addChild(neptune);

	//Earth
	Planet* moon = new Planet(RGB(150, 200, 200), 0.25f, 1.83f, 0.0417f, 0.12f, "planet_textures/texture_moon.jpg"); //Earth's
	earth->addChild(moon);
	//moon->addChild(new ArtificialSatellite(0.015f, 5.0f));

	//Jupiter
	Planet* jupmoon1 = new Planet(RGB(150, 200, 200), 0.125f, 1.40f, 1.54f, 3.0f, "planet_textures/texture_sattelite.jpg");//Jupiter's
	Planet* jupmoon2 = new Planet(RGB(150, 200, 200), 0.125f, 1.88f, 2.12f, 4.3f, "planet_textures/texture_sattelite.jpg");
	Planet* jupmoon3 = new Planet(RGB(150, 200, 200), 0.125f, 2.35f, 5.33f, 5.1f, "planet_textures/texture_sattelite.jpg");
	Planet* jupmoon4 = new Planet(RGB(150, 200, 200), 0.125f, 2.59f, 3.05f, 1.05f, "planet_textures/texture_sattelite.jpg");
	jupiter->addChild(jupmoon1);
	jupiter->addChild(jupmoon2);
	jupiter->addChild(jupmoon3);
	jupiter->addChild(jupmoon4);
	//jupmoon1->addChild(new ArtificialSatellite(0.015f, 10.0f));
	//jupmoon2->addChild(new ArtificialSatellite(0.015f, 3.0f));
	//jupmoon3->addChild(new ArtificialSatellite(0.015f, 2.0f));
	//jupmoon4->addChild(new ArtificialSatellite(0.015f, 15.0f));

	//Saturn
	Planet* satmoon1 = new Planet(RGB(150, 200, 200), 0.139f, 3.0f, 1.1f, 0.16f, "planet_textures/texture_sattelite.jpg"); //Saturn's
	saturn->addChild(satmoon1);
	//satmoon1->addChild(new ArtificialSatellite(0.035f, 5.5f));
	//satmoon1->addChild(new ArtificialSatellite(0.015f, 2.5f));

	//Neptune
	Planet* nepmoon1 = new Planet(RGB(150, 200, 200), 0.25f, 2.551f, 8.79f, 0.02f, "planet_textures/texture_sattelite.jpg"); //Neptune's
	neptune->addChild(nepmoon1);
	//nepmoon1->addChild(new ArtificialSatellite(0.015f, 18.0f));

	vector<vec4> wireframeVertices;
	vector<vec3> wireframeNormals;
	vector<vec2> wireframeTextures;

	//vertices.resize(orbitObjLoader.getFace().size() * 3 + planetObjLoader.getFace().size() * 3);
	//wireframeVertices.resize(cubeObjLoader.getFace().size() * 3);

	planetObjLoader.addVerticesList();
	planetObjLoader.addNormals();
	planetObjLoader.addTextureCoord();
	orbitObjLoader.addVerticesList();
	orbitObjLoader.addNormals();
	orbitObjLoader.addTextureCoord();
	cubeObjLoader.addVerticesList(wireframeVertices);
	cubeObjLoader.addNormals(wireframeNormals);
	cubeObjLoader.addTextureCoord(wireframeTextures);


	//GPU로 보낼 데이터 버퍼 세팅
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	unsigned int bufferIndex = 0;
	unsigned int bufferSize = sizeof(vec4) * vertices.size() * Planet::numberOfPlanets + sizeof(vec3) * normals.size() * Planet::numberOfPlanets + sizeof(vec4) * wireframeVertices.size() + sizeof(vec2) * textures.size() * Planet::numberOfPlanets;
	cout << vertices.size() << endl << normals.size() << endl << textures.size() + 1800 << endl;
	GLuint vertexBuffer;
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, NULL, GL_DYNAMIC_DRAW);

	for (int i = 0; i < Planet::numberOfPlanets; i++)
	{
		glBufferSubData(GL_ARRAY_BUFFER, bufferIndex, sizeof(vec4) * vertices.size(), &vertices[0]);
		bufferIndex += sizeof(vec4) * vertices.size();
	}

	glBufferSubData(GL_ARRAY_BUFFER, bufferIndex, sizeof(vec4) * wireframeVertices.size(), &wireframeVertices[0]);
	bufferIndex += sizeof(vec4) * wireframeVertices.size();

	// Normals
	for (int i = 0; i < Planet::numberOfPlanets; i++)
	{
		glBufferSubData(GL_ARRAY_BUFFER, bufferIndex, sizeof(vec3) * normals.size(), &normals[0]);
		bufferIndex += sizeof(vec3) * normals.size();
	}

	glBufferSubData(GL_ARRAY_BUFFER, bufferIndex, sizeof(vec3) * wireframeNormals.size(), &wireframeNormals[0]);
	bufferIndex += sizeof(vec3) * wireframeNormals.size();

	// Textures
	for (int i = 0; i < Planet::numberOfPlanets; i++)
	{
		glBufferSubData(GL_ARRAY_BUFFER, bufferIndex, sizeof(vec2) * textures.size(), &textures[0]);
		bufferIndex += sizeof(vec2) * textures.size();
	}

	glBufferSubData(GL_ARRAY_BUFFER, bufferIndex, sizeof(vec2) * wireframeTextures.size(), &wireframeTextures[0]);
	bufferIndex += sizeof(vec2) * wireframeTextures.size();

	cout << "enter buffer data has done!" << endl;
	
	program = InitShader("vertex.glsl", "fragment.glsl");
	selectProgram = InitShader("vertex_selected.glsl", "select.glsl");

	/* Shader에 들어가는 Input 값들임 */
	bufferIndex=0;
	//Vertex Position (vPosition)
	GLuint vPosition = glGetAttribLocation(program, "vPosition");
	GLuint vsPosition = glGetAttribLocation(selectProgram, "vPosition");
	glEnableVertexAttribArray(vPosition);
	glVertexAttribPointer(vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(bufferIndex));
	bufferIndex += sizeof(vec4) * (vertices.size() * Planet::numberOfPlanets + wireframeVertices.size());

	//Vertex Normal (vNormal)
	GLuint vNormal = glGetAttribLocation( program, "vNormal" );
	glEnableVertexAttribArray(vNormal);
	glVertexAttribPointer(vNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(bufferIndex));
	bufferIndex += sizeof(vec3) * (normals.size() * Planet::numberOfPlanets + wireframeNormals.size());

	//Vertex Texture Coordinate (vTexCoord)
	GLuint vTexCoord = glGetAttribLocation(program, "vTexCoord");
	glEnableVertexAttribArray(vTexCoord);
	glVertexAttribPointer(vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(bufferIndex));
	bufferIndex += sizeof(vec2) * (textures.size() * Planet::numberOfPlanets + wireframeTextures.size());
	
	AmbientProduct = glGetUniformLocation(program, "AmbientProduct");
	printf("AmbientProduct: %d\n", AmbientProduct);
	DiffuseProduct = glGetUniformLocation(program, "DiffuseProduct");
	SpecularProduct = glGetUniformLocation(program, "SpecularProduct");
	LightPosition = glGetUniformLocation(program, "LightPosition");
	Shininess = glGetUniformLocation(program, "Shininess");
	IsShading = glGetUniformLocation(program, "IsShading");
	UseTexture = glGetUniformLocation(program, "UseTexture");
	IsEarth = glGetUniformLocation(program, "IsEarth");
	EnableCloud = glGetUniformLocation(program, "EnableCloud");
	ModelView = glGetUniformLocation(program, "ModelView");
	Projection = glGetUniformLocation(program, "Projection");
	LookAtMat = glGetUniformLocation(program, "LookAt");

	sModelView = glGetUniformLocation(selectProgram, "ModelView");
	printf("sModelView: %d\n", sModelView);
	sProjection = glGetUniformLocation(selectProgram, "Projection");
	printf("sProjection: %d\n", sProjection);
	sLookAtMat = glGetUniformLocation(selectProgram, "LookAt");
	sCode = glGetUniformLocation(selectProgram, "code");

	textureLocation = glGetUniformLocation(program, "TextureColor");
	textureLocationNight = glGetUniformLocation(program, "TextureColorNight");
	textureNormalMap = glGetUniformLocation(program, "TextureNormalMap");
	textureSpec = glGetUniformLocation(program, "TextureSpec");
	textureCloud = glGetUniformLocation(program, "TextureCloud");
	EarthTime = glGetUniformLocation(program, "EarthTime");

	glEnable(GL_DEPTH_TEST);
	selectedPlanet = 0;
}

void keyDown(unsigned char key, int x, int y)
{
	controller.keyDown(key, x, y);
}

void keyUp(unsigned char key, int x, int y)
{
	controller.keyUp(key, x, y);
}

void specialKeyDown(int key, int x, int y)
{
	controller.specialKeyDown(key, x, y);
}

void specialKeyUp(int key, int x, int y)
{
	controller.specialKeyUp(key, x, y);
}
#define BUFSIZE 512
void mouse(GLint button, GLint action, GLint x, GLint y)
{
	if (button == GLUT_LEFT_BUTTON && action == GLUT_DOWN)
	{
		selectedPlanet = processSelection(x, y);
	}
	if (selectedPlanet == 0)
	{
		controller.mouse(button, action, x, y);
	}
}

void mouseMove(int x, int y)
{
	controller.mouseMove(x, y);
}
void timer(int input)
{
	controller.launchRocketOnCountdown();

	if (controller.movingUp)
		camera.move(Camera::UP);
	else if (controller.movingDown)
		camera.move(Camera::DOWN);
	if (controller.movingLeft)
		camera.move(Camera::LEFT);
	else if (controller.movingRight)
		camera.move(Camera::RIGHT);

	if (controller.zoomingIn)
		camera.zoomIn();
	else if (controller.zoomingOut)
		camera.zoomOut();
	glutTimerFunc(1, timer, 0);
	glutPostRedisplay();
}
void main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB |GLUT_DEPTH);
	glutInitWindowSize( 500, 500 );
	glutInitWindowPosition( 0, 0 );
	controller.setWindowID(glutCreateWindow("BH Bros' Assignment1"));
	glutDisplayFunc( display );
	glutKeyboardFunc( keyDown );
	glutKeyboardUpFunc(keyUp);
	glutSpecialFunc( specialKeyDown );
	glutSpecialUpFunc( specialKeyUp );
	glutMouseFunc(mouse);
	glutMotionFunc(mouseMove);
	glutReshapeFunc( reshape );
	timer(0);
	glewInit();
	init();
	glutMainLoop();

}
